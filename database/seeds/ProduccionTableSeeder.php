<?php

use Illuminate\Database\Seeder;

class ProduccionTableSeeder extends Seeder{

    public function run()
    {
        $faker = Faker\Factory::create();

        $date = new DateTime('2016-01-01');

        $jsonControl = new \stdClass();
        $jsonControl->week = 0;
        $jsonControl->last_update = $date->format('Y-m-d');
        $jsonControl->produccion = [];

        foreach(range(1, 10) as $index) {

            $date->add(new DateInterval('P7D'));
            $produccion = new \stdClass();
            $produccion->week = $jsonControl->week + 1;
            $produccion->nro_huevos =3000;
            $produccion->poblacion = 3500;
            $produccion->mortalidad = 1;
            $produccion->kg_alimentos = 2000;
            $produccion->conversion_alimenticia = 200;
            $produccion->porcentaje_productividad = 85.23;
            $produccion->gramo_ave = 200;
            $produccion->peso_total_huevos = 30;
            $produccion->date = $date->format('Y-m-d');

            array_push($jsonControl->produccion, $produccion);
            $jsonControl->week++;
            $jsonControl->json_control = json_encode($produccion);
        }

        foreach(range(1, 23) as $index)
        {
            \DB::table('produccions')->insert([
                'json_control'  => json_encode($jsonControl),
                'galpon_id'     => $index
            ]);
        }

    }
} 