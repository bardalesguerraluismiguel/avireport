<?php namespace AviReport\Entities;

use Illuminate\Database\Eloquent\Model;

class Produccion extends Model {

    protected $fillable = ['json_control', 'week', 'galpon_id'];

}
