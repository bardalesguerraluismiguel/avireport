@extends('layout_admin')

@section('content')
    <section class="content-header">
        <h1>
            FAQ's
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <!-- /.row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo crear una granja?</h3>
                        <UL>En el menú contextual de la izquierda, selecciona <b>Granjas</b>. A continuación, selecciona <b>Nueva Granja</b> para despues llenar con tus datos y finalmente
                        pulsar <b>Registrar</b>.</UL>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo acceder a un galpón?</h3>
                        <UL>En el menú contextual de la izquierda, selecciona <b>Granjas</b>. A continuación, en la lista de Granjas Supervisadas, seleccionada la granja a la cual quieras ingresar un nuevo galpón;
                            puedes hacerlo a traves del nombre de la granja, o pulsando el boton <b>Ingresar</b> de la columna Acciones.</UL>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo crear un galpón?</h3>
                        <UL>Cuando hayas accedido a la granja. Selecciona <b>Nuevo Galpón</b>. Ingresas los datos requeridos del galpón, como numero de aves
                        y una breve descripcion. Una vez llenado todos los campos, pulsar <b>Guardar</b> y ya estaras listo para acceder al control.</UL>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo acceder al Control Semanal de un Galpón?</h3>
                        <UL>En la lista de galpones, seleccione el galpón. A continuación, podrá seleccionar entre el <b>Control de Crecimiento</b> o <b>Control de Producción</b>.
                         La segunda opción se activará solo si la semana de crecimiento del galpón se encuentra mayor en la <b>semana 18.</b></UL>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo acceder al Control de Crecimiento?</h3>
                        <UL>Cuando hayas accedido al galpón, selecciona <b>Control de Crecimiento</b>. Desde aqui podras acceder al gráfico general de crecimiento,
                        el gráfico de Uniformidad del galpón. Ademas estan las opciones del control de la semana y la <b>exportación de los gráficos a Excel</b>.</UL>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿Cómo puedo acceder al Control de Producción de un Galpón?</h3>
                        <UL>Cuando hayas accedido al galpón, selecciona <b>Control de Producción</b>. Aqui podras observar el control semanal de la produccion del galpón.
                        Además, podrás importar los datos de la semana.</UL>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-body">
                        <h3 style="text-align: center">¿ALGUNA PREGUNTA ADICIONAL?</h3>
                        <p>No dude en enviarnos un mensaje a idea.tru@gmail.com. Tambien puede consultar nuestro FAQ para mayor informacion.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection

    @section('css-content')
            <!-- CSS -->
    @endsection


    @section('js-content')
            <!-- JS -->
@endsection
