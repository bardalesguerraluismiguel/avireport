@extends('layout_admin')

@section('content')
<section class="content-header">
    <h1>
        Dashboard
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $info['numGranjas'] }}</h3>

                    <p>Granjas en Total</p>
                </div>
                <div class="icon">
                    <i class="fa fa-institution"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $info['numGalpons'] }}</h3>

                    <p>Galpones en Total</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <!--<a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
        </div>
        <!--
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>44</h3>

                    <p>Clientes</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">Más Información<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        -->
        <!-- ./col -->
        <!-- <div class="col-lg-4 col-xs-4">
            <!-- small box ->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $info['numControls'] }}</h3>

                    <p>Controles Realizados</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col ->
    </div> -->
    <!-- /.row -->
   <div class="row">
        <div class="col-md-12">
            <div class="box box-body" style="text-align: center">
                <script type="text/javascript">
                    var tweenui_cid = "94c35558-e088-45ae-a165-46aec932e25f";
                    var tweenui_pid = "f47741dd-abbf-4118-9284-ec8abfbdb20e";
                    var tweenui_width = 460;
                    var tweenui_height = 230;
                </script>
                <noscript>Activate javascript to view banner ad created with tweenui <a href="http://tweenui.com/">mobile banner maker</a></noscript>
                <script src="https://s3-eu-west-1.amazonaws.com/display.tweenui.com/show.js" type="text/javascript"></script>
            </div>
        </div>
    </div>
</section>
@endsection

@section('css-content')
<!-- CSS -->
@endsection


@section('js-content')
<!-- JS -->
@endsection
